import RPi.GPIO as GPIO ## Import GPIO library
import time ## Import 'time' library. Allows us to use 'sleep'


GPIO.setwarnings(False)


GPIO.setmode(GPIO.BOARD) ## Use board pin numbering

GPIO.setup(11, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(11,0)
GPIO.setup(13, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(13,0)
GPIO.setup(15, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(15,0)



try:
    while(True):
        request = raw_input("RGB-->")
        if (len(request) == 3):
            GPIO.output(11,int(request[0]))
            GPIO.output(13,int(request[1]))
            GPIO.output(15,int(request[2]))

except KeyboardInterrupt:
    GPIO.cleanup()


