import RPi.GPIO as GPIO ## Import GPIO library
import time ## Import 'time' library. Allows us to use 'sleep'

GPIO.cleanup()


GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
GPIO.setup(11, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(11,0)
GPIO.setup(13, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(13,1)
GPIO.setup(15, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.output(15,1)


sleep(5)

GPIO.cleanup()

